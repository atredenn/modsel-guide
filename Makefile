MSDIR = ./manuscript
CODEDIR = ./code
BIBFILE = ./manuscript/lemonade_ms.bib

.PHONY: all
all: $(MSDIR)/modsel-guide-ms.pdf


## Modeling for exploration
EXP_DEPS = $(CODEDIR)/explore_butterflies.R
EXP_TARG = $(CODEDIR)/explore_butterflies.Rout

$(EXP_TARG): $(EXP_DEPS)
	Rscript $< >$(CODEDIR)/explore_butterflies.Rout


## Modeling for understanding
UND_DEPS = $(CODEDIR)/understand_butterflies.R
UND_TARG = $(CODEDIR)/understand_butterflies.Rout

$(UND_TARG): $(UND_DEPS)
	Rscript $< >$(CODEDIR)/understand_butterflies.Rout


## Modeling for prediction
PRED_DEP = $(CODEDIR)/predict_butterflies.R
PRED_TARG = $(CODEDIR)/predict_butterflies.Rout

$(PRED_TARG): $(PRED_DEP)
	Rscript $< >$(CODEDIR)/predict_butterflies.Rout

## Compile the manuscript
MS_DEPS := $(MSDIR)/modsel-guide-ms.Rmd
MS_DEPS += $(CODEDIR)/explore_butterflies.Rout
MS_DEPS += $(CODEDIR)/understand_butterflies.Rout
MS_DEPS += $(CODEDIR)/predict_butterflies.Rout
MS_DEPS += $(BIBFILE)

$(MSDIR)/modsel-guide-ms.pdf: $(MS_DEPS)
	Rscript -e 'rmarkdown::render("$<")'
