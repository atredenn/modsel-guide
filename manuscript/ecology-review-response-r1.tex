\documentclass[11pt]{article}

\usepackage{amsmath,amsfonts,amssymb,graphicx,natbib,setspace,authblk} 
\usepackage{float}
\usepackage[running]{lineno}
\usepackage[vmargin=1in,hmargin=1in]{geometry}

\usepackage{enumitem} 
\setlist{topsep=.125em,itemsep=-0.15em,leftmargin=0.75cm}
\setlength{\parindent}{0.0in} 
\setlength{\parskip}{0.12in}
\usepackage[compact,small]{titlesec} 

\usepackage[sc]{mathpazo} %Like Palatino with extensive math support

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%% Just for commenting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\usepackage[usenames]{color}
\definecolor{ForestGreen}{rgb}{0.1,0.44,0.1} 
\definecolor{blue}{rgb}{0,0,0.7} 
\definecolor{red}{rgb}{0.8,0,0}

\newcommand{\new}{\textcolor{red}} 
\newcommand{\comm}{\textcolor{ForestGreen}}
\newcommand{\response}{\textcolor{blue}}
\newcommand{\reply}{\textcolor{blue}}


\renewcommand{\floatpagefraction}{0.98} 
\renewcommand{\topfraction}{0.99} 
\renewcommand{\textfraction}{0.05}
\clubpenalty = 10000 
\widowpenalty = 10000

\newcommand{\be}{\begin{equation}} 
\newcommand{\ee}{\end{equation}} 
\newcommand{\ba}{\begin{equation} \begin{aligned}} 
\newcommand{\ea}{\end{aligned} \end{equation}}

\def\X{\mathbf{X}} 
\def\A{\mathbf{A}} 
\def\B{\mathbf{B}} 
\def\C{\mathbf{C}} 
\def\D{\mathbf{D}}
\def\G{\mathbf{G}} 
\def\H{\mathbf{H}} 
\def\N{\mathbf{N}} 
\def\M{\mathbf{M}} 
\def\W{\mathbf{\Omega}}
\def\P{\mathbf{P}} 
\def\V{\mathbf{V}} 
\def\r{r^{\vphantom{0}}} 
\def\rbar{\bar{r}^{\vphantom{0}}}
\def\E{\mathbb{E}}

\newcommand{\s}[1]{{#1}^{\#}} 
\newcommand{\f}[1]{{#1}^{\flat}} 
\newcommand{\br}[1]{\langle {#1} \rangle}
\newcommand{\bs}{\backslash}

\begin{document} 

\centerline{\large{Response to Reviews}} 

\hrulefill

\footnotesize{Manuscript number: ECY20-0414} \\
\footnotesize{Title: A practical guide to selecting models for exploration, inference, and prediction in ecology} \\
\footnotesize{Author(s): Tredennick, Andrew; Hooker, Giles; Ellner, Stephen; Adler, Peter}

\hrulefill

\normalsize 

\normalsize 

October 8, 2020

Dear Dr. Johnson,

Thank you for the opportunity to revise our manuscript. We are grateful to you and the reviewers for finding value in our work, and for critiques that improved the manuscript. One of the biggest changes we made is, ironically, the hardest to detect: we overhauled our analysis for the prediction example, following Reviewer 2's suggestions, but none of the results or conclusions changed. Our other revisions are generally responses to the reviewers' semantic and philosophical concerns (your words), as described in our point-by-point responses below. The original reviews are in black and \response{our responses are in blue.} We do note that these revisions increased the length of the paper from roughly 7200 words to 7600 words. The revisions are shown in blue text in the manuscript.

Thank you for your time, 

Peter Adler on behalf of all coauthors

\subsection*{Editor} 
Thank you very much for submitting your manuscript "A practical guide to selecting models for exploration, inference, and prediction in ecology" ECY20-0414 to Ecology. The reviewers and I appreciate the work you have accomplished. I am willing to consider a revised version for publication in the journal, assuming that you are able to modify the manuscript according to the recommendations.

Reviewer 1 was laudatory of the manuscript with one major suggestion that you expound a bit on the 'dark side' of AIC to better clarify your position on the pros and cons this criterion. Reviewer 2 commended you on tackling an important problem, however, they had a number of major concerns that need to be addressed before I am satisfied that the manuscript is suitable for publication in Ecology. Some of the concerns may boil down to semantics, while others are philosophical in nature. As for me, my first impulse was that ``there are already enough books and papers on the basics of model selection.'' However, I see value in straightforward guidance that emphasizes selecting a model based on the goal of the study, for myself included.

\subsection*{Referee 1} 

I really enjoyed reading this paper and think it will be an invaluable contribution for its clear description of model selection, an issue many ecologists struggle with. The paper strikes a good balance between describing current practices while providing a window into some underutilized but very promising approaches in model selection (e.g. regularization). I also appreciated the retrospective view on the group's previous work (including model regrets!).

\reply{Thank you for the kind words, and we are glad you found the paper helpful.}

My major suggestion is to be more upfront about the downsides to AIC in the section beginning on lines 221. This introduction to AIC does not mention its asymptotic assumptions or its inability to account properly for random effects with unknown degrees of freedom. This section also states, "Akaike’s original purpose was to approximate a model’s out-of-sample predictive skill, using only the data used to fit the model," which is sort of true, but also ignores the potential for leave-one-out approaches on small datasets.

Strangely, the manuscript addresses these concerns in the prediction Example section (starting on lines 461), where it off-handedly mentions AIC's asymptotic assumptions. This example presents a really nice alternative to AIC with approaches that I think will be new to many ecologists (leave-one-out and regularization). The manuscript would be stronger if these problems with AIC were foreshadowed a bit more back in the section starting on line 221. I also found myself wishing that some of the very well-written text in Appendix C (out-of-sample validation) was in the main text, as it describes these methods in a lucid way that will be useful for many.

\reply{This is a good point. We have added text to the section on AIC to foreshadow what comes in the Example sections. Specifically, earlier in the manuscript, on lines 282-289, we now mention the asymptotic assumptions of AIC and how that can impact model selection. We appreciate the kind words about Appendix C and we hope many find the details useful, but given page constraints and the large topic we are covering, we have decided to leave these descriptions in the Appendix.}

I also think the paper could be stronger if there were a few sentences linking the three frameworks for statistical inference to philosophy of science. Citing some of Karl Popper's work is an obvious choice, but there are also epistemological arguments that science advances through prediction, rather than hypothesis testing (see Imre Lakatos).

\reply{This is an interesting suggestion, but we prefer not to advocate for a particular modeling goal. We expect that ecologists will continue to create models for all three goals and we do not want our advice to be ignored out-of-hand if we advocate for one goal over another (e.g., by arguing the point that science advances most rapidly through prediction). We also feel that doing this argument justice would require much more space than we have in an already long manuscript.}

Richard McElreath's ``Statistical Rethinking'' book covers a lot of the same ground as this paper and would be a good reference for readers looking for more information.

\reply{This is an excellent suggestion and we regret not citing McElreath in our original submission. We have added citations in lines 50, 306, 332 and 366. "Statistical Rethinking" is geared toward a general introduction to Bayesian statistical modeling (for hypothesis testing and prediction), so we tend to cite it in our Bayesian sections. As you say, we cover some of the same ground, but we still think having a paper focused solely on model selection is valuable.}

Ferguson et al. 2017's paper titled ``Detecting population–environmental interactions with mismatched time series data'' presents another solution to the problem of detecting weather effects via population time series and could be mentioned.

\reply{Thank you for pointing out this interesting paper, which none of us had read before. Ferguson et al. present an interesting method for identifying  the time scales over which environmental variables impact population dynamics. But the paper does not provide clear advice on how to select \emph{which} environmental covariate to use. Still, the paper is very relevant and we have cited it on line 74 along with Teller et al. 2016 because Teller et al. is a special case of the approach described by Ferguson et al., which can be considered functional linear models to describe peaks of influence}. 

line 249-- Aki Vehtari's research has made a convincing case for replacing WAIC with a different -IC metric for Bayesian stats, pareto-smoothed importance sampling: https://arxiv.org/abs/1507.04544. At least for the Stan user community, WAIC is old news; probably useful to mention this in the text.

\reply{Good point. We have added a reference to the LOO-IC metric as the new, preferred alternative to DIC and WAIC (lines 302-306).}

\subsection*{Referee 2}

Ecologists have long used and been fascinated with model selection. However, as the author’s argue, the explicit goals are rarely clear when model selection is used in ecological studies. The authors present a strong argument for the “three distinct goals” for modeling of ecological data and then connect these goals to model/covariate selection techniques. The authors then provide three worked examples that are intended to serve the practicing ecologists.

I congratulate the authors on tackling an important philosophical problem. I am an applied statistician that works primarily on ecological problems, and I have also noticed the lack of clarity around the “three distinct goals” when working with ecologists on data analyses. That said, I wanted to love this manuscript. There are certainly some very important, well-stated problems the authors address and resolve. For example, I loved the section on line 107–185. But then there were many parts of the manuscript that I disliked. There is also some advice and practices suggested by the authors that I disagree with and would not recommend. Rarely when reading a manuscript do I fnd myself having such a strong positive and negative reactions. Therefore my goal with this review is to offer a path forward with revisions by offering specifc comments/concerns that I feel need to be addressed.

\reply{Thanks for the positive comments, and for the critical ones. Your review has improved the manuscript.}

1. Overall this paper is almost exclusively focused on covariate selection and does little or nothing to help ecologists trying to do model selection. I understand that ecologists think model selection and covariate selection they are the same thing, but in the stats literature there are way more recent papers on covariate selection. Give that the author’s work almost exclusively focuses on covariate selection, it seems misleading to sell it as model selection.

\reply{This an interesting comment and made us think more critically about what "model selection" is. The reviewer is correct that model selection encompasses more than covariate selection -- for example, covariate transforms (a form of covariate selection), model functional forms, and distributional assumptions. However, as the reviewer also notes, model selection in ecology most often refers to covariate selection, and this guide is meant to be by and for ecologists. So it makes sense, in our view, that the paper focuses on covariate selection. All of our advice is equally valuable for other kinds of model selection; we now explicitly acknowledge this on lines 107-111, but retain our focus on covariate selection.}

2. The data example, which is the main contribution of the manuscript, promotes a number of very poor statistical practices. For example, the response variable in the data example is Rt = log10(Nt+1 /Nt) which creates a number of well known problems. Although I realize that ecologists still do data analysis this way, it is reminiscent of statistical practices used in the 1970. Why not ft a model to Nt? This would eliminate many poor practices by the authors. For example, in Equation 4 the authors insert a transformation of Nt as a predictor of Rt. Models that do this are not valid since the response contains the predictor (i.e., Rt includes Nt in it).

\reply{As suggested, for the inference and prediction examples, we fit a model to Nt instead of Rt. None of the conclusions or interpretations have changed. We originally modeled Rt to match the previous analyses of the butterfly data set, but we agree that updating to a Gompertz-style population model is better practice. See line 492.}

3. There are many error in the statistical notation. I've caught some of these and offered possible corrections in the specific comments below.

\reply{Thank you for the careful review. We have updated the notation on line 492. We retain our notation on line 211 (eq. 1) because the text around the equation explains why the equation is written as it is (it refers to a fitted regression). We respond to each in detail below.} 

4. Line 47: I was perplexed by the use of ``high dimensionality.'' It seems that the issue at hand is temporal misalignment of a high temporal resolution predictor variable. See for example Pacifici et al. (2019).

\reply{Thank you for pointing out the Pacifici et al. paper. However, the problem we outline is more than just alignment, though misalignment is certainly part of the problem. The larger issue is that the effects we are concerned with can actually be \emph{distributed over time lags}, making it a high dimensional problem. We have added the Pacifici et al. citation on lines 71-72 as it is an important contribution.}

5. Line 53–54: The authors state that ``we must either aggregate the weather variables over some time period (van de Pol et al., 2016), or fit complex functional linear models to capture peaks of influence.'' This is one of those statements that made by blood boil! If this is suppose to be a guide, why would anyone recommend these as the only two options? The first option (temporal aggregation) will basically lead to the spatial equivalent of the location error or the ecological fallacy. The second option makes little or no sense to me. I would suggest that the author’s take a look at the environmental statistics and biostatistical literature. There are way more options in situations like this. For example, distributed lag models are regularly used with high temporal resolution covariates. Heck even within the ecological statistics literature there are some new methods being proposed (e.g., Elston et al. 2017). As another example, and in reference to above, if the ``high dimensionality'' problem was recast as a temporal misalignment or change of support problem, then there would be may be solid theoretical reasons to aggregate predictors.

\reply{We have added the mention of temporal misalignment (see above) and we have also made clear that summary statistics of the time series can be user-defined (line 69). In addition, we have cited the very interesting paper of Elston et al. 2017 on that same line as an example of using a summary statistic of the series, like the frequency component. As described in Teller et al. 2016, functional linear models are actually quite general, so we retain our mention of them as an omnibus solution to this particular problem. Indeed, Elston et al. represents the functional coefficient as a sum of four sinusoids, demonstrating the generality and utility of functional linear models. Therefore, while it may sound like there are only two options, the possibilities within those options are myriad. Given space limitations, however, we decided to keep this section as concise as possible.}

6. Lines 54–61: Although I agree with the list of potential problems here (e.g., to short of time series, non­linearity) it seems that the two biggest problems are not mentioned. In my experience I would say collinearity and spatial and temporal confounding are the two biggest problems. Given that spatial or temporal confounding is somewhat new to ecologists it seems that a modern paper on covariate selection should mention this (e.g., Hodges and Reich 2010; Fieberg and Ditmer 2012; Hefley et al. 2016).

\reply{Good point. We focused so intently on covariates that we failed to mention the importance of meeting statistical assumptions. We contend that meeting statistical assumptions is an issue that spans any and all statistical modeling goals. Therefore, we mention these important points on lines 82-83, but refrain from wading too deeply into the issues. In other words, our focus is model form, where it is easiest to develop many, many competing models; meeting basic statistical assumptions about correlation structures is important, but there are many clear (even if difficult) ways to deal with such issues. Lastly, we note that our discussion of regularization SI Section B) mentions collinearity.}

7. Lines 63–64: The author’s state that “The temptation is to.....” This is an excellent point and I could not agree more. I think the authors should expand on this because the poor practice threatens the validity and reproducibility of science since repeated studies won’t find the same effects. I would be good to remind ecologists of current and ongoing ``reproducibility crisis.``

\reply{This is an excellent suggestion. We have added text on line 87.}

8. Line 68 and numerous other places: The authors’ say ``relatively few independent observations,'' but I don’t actually know what they mean. Do we ever have independent observations with ecological data? For example, the authors use time-series in many heuristic explanations (e.g., lines 48–49), but the only way to get ``independent observations'' is to wait a really long time (i.e., chose the temporal resolution of data collection so that the observations are not longer autocorrelated). It seems that the more appropriate way to address these issues is to acknowledge spatial and temporal autocorrelation because most ecological data are spatio-temporal. To me the notion of ``independent observations'' died a long time ago. The reality with most ecological data is that we can almost always assume that we never have independent observations and we must acknowledge the fact that effective sample size may be low and we may need to build models that explicitly account for the autocorrelation.

\reply{Good point. We have added a citation to Hodges et al. 2010 to these statements (lines 82 and line 91), referencing the importance of modeling autocorrelation as needed.}

9. Lines 90–92: This isn’t true. It is also somewhat spinning an old problem ecologists are aware of (e.g., data snooping or dredging as described by Burnham and Anderson 2002). As long as you make all the modeling decisions \emph{a priori}, then you are on solid ground. The issue is that inference after model selection, using standard statistical techniques, is not valid. Breiman (1992) referred to the use of classical tools for post-selection inference as a ``quiet scandal'' in the statistical community. This might be a good quote to drive home the point to ecologists.

\reply{This comment shows that our language was not clear enough. We have revised lines 90-92 (now lines 118-120) to read: ``Common statistical tests, applied to any one model, do not account for the fact that model selection violates assumptions on which the calculation and interpretation of P-values depend.''}


10. Lines 93–96: I think it would be a lot less confusing to just say that development of methods that are valid after model selection is an open area of research in statistics. I also think it is remiss to not mention that Bayesian methods are on much more solid ground here. For example, Bayesian model averaging has a theoretical solid foundation to make inference that accounts for model selection/uncertainty whereas the likelihood-based (frequentists) approaches do not.

\reply{Good point. We have revised the text as suggested and added reference to Bayesian model averaging on lines 126-128, citing Hoeting et al. 1999.}

11. Lines 148–149: That authors state that ``Inference does not require validation on independent data, because the goal is to determine whether or not a particular data set provides sufficiently strong evidence for a hypothesis.'' I think they need to expand on this. As written the statement is confusing and is incorrect. For example, the statement should say that ``Statistical inference for a single data set...'' Inference in general usually requires more than one study/data set.

\reply{We completely agree that inference requires more than one study to be considered definitive. But, formalized inference procedures are strictly defined for only one data set. We have added the following text to clarify this argument and to mention that one study should not be considered definitive even if the model was ``set up'' for inference: ``Statistical inference from a single data set is only one part of the process of developing scientific knowledge. It assesses the reliability of statements about a particular set of data obtained by particular methods under particular conditions. Any conclusions obtained via statistical inference thus require replication and validation across a range of conditions before they are accepted as scientific fact.''}

12. Line 149–152: This is where the manuscript feels completely unoriginal. Isn’t this what Burnham and Anderson suggested 20+ years ago?

\reply{It is unoriginal. But ecologists (ourselves included) continue to overlook this issue. We have revised to be a little more original by examining what we mean by a ``small set" of competing models (lines 186-190): ``But what exactly is a “small set”? If the set includes just two models, we are clearly on safe ground for inference. Tests of some hypotheses may require three models. However, as the number of models grows, we should be increasingly skeptical about whether inference is truly the goal of the analysis, rather than exploration or prediction." }


13. What is stated on lines 172–173 contradicts what is stated on lines 158–159.

\reply{We should have been clearer. The statement on lines 158-159 of the original submission was meant to say that understanding the process should in general improve prediction, while the statement on lines 172-173 of the original submission says that the optimal model for prediction may not always be suitable for inference. In other words, understanding will improve predictions (in general), but the best predictions may not always improve understanding. We have revised both sentences (lines 202-204 and lines 226-227).}

14. What is stated on lines 283–284 (i.e., “it is imperative to do out-of-sample validation”) contradicts what the authors do in practice (i.e., cross-validation).

\reply{Starting on line 297 of the original submission, we discuss the virtues of cross-validation for small data sets. However, we did not make it clear on lines 283-284 that out-of-sample validation is an umbrella term that includes cross-validation because you are validating against data not used to fit the model (at least within a fold). We have revised this text for clarity (lines 337-338) and refer the reviewer to lines 350-362 as well.}

15. Lines 297–299: I don’t think this is good advice. For example, cross-validation has a ton of problems with small data sets. The problems with cross-validation when sample size is small is one of the reasons one would prefer to use an information criterion (e.g., AIC) to approximate out-of-sample predictive ability.

\reply{In our view, whether one prefers information theoretic criteria (IC) or cross-validation (CV) is a bit subjective, depending on how the researcher perceives ``small''  and how they handle the trade-offs associated with both IC and CV. With small datasets, the problems with CV are that the sample size can change between folds and the errors are correlated but not accounted for. As for IC, there are the asymptotic assumptions and the strong dependency on model assumptions. Which is better? Hard to say. But clearly we should expand on these trade-offs more. We have done so in this section (lines 357-362), with special focus on caveats about both approaches.}

16. 304–306: Scoring functions are probably one of the most important topics to cover and ecologists know very little about them. I think the authors need to spend more time introducing scoring functions for out-of-sample measures of predictive accuracy.

\reply{This is fair criticism. We glossed over scoring functions because we feel they are treated well in other papers. However, we failed to point readers to those papers or to mention why they should go read them! This has been remedied on lines 363-368.}

17. What is stated on lines 573–774 contradicts what is on lines 148–149.

\reply{The new sentence we added in response to comment 11 (above) solves the apparent contradiction.}

\emph{Specific comments}

1. Line 4: The use of the word “paralyzing” is ableist. How about confusing or disorientating?

\reply{Good point, we revised to "confusing."}

2. Line 22–23: Why should ecologists expect an answer to the question of “what model selection approach I should use?” Perhaps, we (statisticians and quantitative ecologists) should be trying to educate ecologists about the differences rather than prescribe approaches.

\reply{Fair point. We have changed this to: ``how should we decide which model selection approach to use?'' We think this adequately side-steps the issue.}

3. Line 22–23: I think the authors missed some very key literature here (e.g., Ver Hoef and Boveng 2015). Why do ecologists need model selection? There seems to be an obsessive focus on model selection in ecology, but when I interact with other disciplines that are somewhat similar to ecology (e.g., entomology), they seem less obsessed and more focused on things equally matter (e.g., proper experimental design, evaluation of predictive performance, preferential sampling, etc). It would be good to provide some background why model selection became some important and complicated in ecology.

\reply{Good point. We have added to this section on lines 23-41, highlighting the need for model selection in ecology where experiments that mimic real-world conditions at relevant spatial and temporal scales is exceedingly difficult. Thus, much of ecology focuses on competing models to tease out insight from noisy observational data. We have also added a citation to Ver Hoef and Boveng 2015 on line 25.}

4. Line 32: I think you need to give an example how machine learning literature explicitly considers the goal of modeling. As written it sounds a bit condescending, but without example it is not clear how exactly the folks in machine/statistical learning are doing things so much better (which I doubt they are).

\reply{We edited this sentence lightly and provided more citations supporting the claim (lines 49-51).}

5. Line 55–56: The effect of all covariates are probably nonlinear. Why bother overwhelming the reader with real world issues that we usually ignore?

\reply{This is true, but many ecologists focus specifically on nonlinear effects through data transforms that produce curves. We have decided to leave this text as is.}

6. Line 60–61: Isn’t that why ecologists almost always rely (implicitly) on the space-for-time substitution hypothesis? Based on my experience the time-series are so short that we might as well give up trying to learn from the data unless we have only a small number of covariates.

\reply{While we agree this is challenging, we don't plan to give up! Predicting and understanding population dynamics requires time series methods, so it is worth mentioning and knowing our current limits. We have kept this text as is.}

7. Line 67–68: “selecting among models that link weather to biological responses represents a worst-case scenario for model selection...” This seems pretty oversold. I think it is a difficult situation, but certainly not the worst case.

\reply{We have added ``in ecology'' to the end of this statement and modified to read: ``represents one of the worst-case scenarios for model selection in ecology:'' (line 90).}

8. Line 87–90: This is a very long and difficult to understand sentence. Please revise this.

\reply{Done. See lines 115-120.}

9. Line 97: I have no clue what a “relatively restricted models” is. Perhaps give an example.

\reply{Done. Revised to: ``these methods apply to particular models and methods, e.g. using LASSO to select covariates in a linear model.'' See line 125.}

10. Line 97–98: “currently no consensus in the statistics community about what constitutes correct practice” This is vague and I doubt there will ever be consensus about what is correct practice. I think we (statisticians) actually know more that the authors state. For example, there is near consensus of what is poor practice (e.g., standard inference after model selection) and what is best practice (e.g., Bayesian model averaging). Sure we don’t have consensus about what is correct practice, but we don’t have that for most everything in statistics.

\reply{Good point. We have rephrased (lines 128-131): ``There is currently no consensus in the statistics community about what constitutes correct practice, and there likely never will be. While this may be dispiriting to ecologists, there is broad consensus on what constitutes poor practice, providing guidelines for model selection that we discuss here.''}

11. Line 99-101: I have no clue what “researcher degrees of freedom is.” I think a better explanation is needed here. Basically, researcher end up “overfitting” or “running out of degrees of freedom” when they use the data for multiple purposes. For example, using the data to find the “optimal” transformation of the response, and then using the same data to preform covariate selection, and finally inference. Because of all these steps, you’ve almost guaranteed to find a model that fits the data at hand a little to good and that won’t perform well on a new data set (i.e., inference will be di.erent or predictive power will be lower).

\reply{Despite our lack of clarity, your description is exactly researcher degrees of freedom. We have added a brief definition and citation (line 134).}

12. Line 105–106: This is one of those statements that makes my blood boil! The issue is much bigger than p-values so it is misleading to suggest that it applies to p-values. By reading this ecologist will think they can just avoid p-values and then avoid the issues.

\reply{We have rephrased to ``It is always important to understand what actions an inference procedure accounts for in calculating \emph{uncertainty estimates such as p-values or confidence intervals}, and what constitutes data exploration beyond those boundaries.'' See lines 138-140.}

13. Lines 129–130 and 135–136: While I do agree with the general statements here. However, isn’t failing to discover a significant relationship when generating a hypothesis just as dangerous as following a spurious hypothesis. For example, it seems hypotheses that are based on spurious correlations will most likely end when a second data set is collected and analyzed. This happens all the time in science. It is an inefficiency, but there isn’t a great way around it. On the other hand not pursuing a hypothesis because it isn’t significant is probably more damaging. I get that your trying to provide practical advice, and I do agree Ecologists should be concerned and account for multiple comparisons, but at the end of the day this is a difficult problem to solve and it feels like you’re offering a simple solution which only protects against false discoveries.

\reply{We conclude this section with the following text: ``Finally, if we clearly communicate that our goal is exploration, meaning the generation but not the testing of hypotheses, the consequences of false discoveries are minimized. By assiduously avoiding any claims of confirmation, we can emphasize that the proposed hypotheses should not be accepted until they are tested with independent data.'' We think this clearly articulates the very comment and suggestion made by the reviewer. See lines 170 - 173.}

14. Line 158–159: A good case study of this is given in Hefley et al. (2017). Also, I think it would be good to give more information as to why models that include our best understanding of a process should produce better forecasts. Basically they require fewer parameters because more (correct) assumptions are baked into the models.

\reply{We have added a brief (one sentence) discussion elaborating on this topic and included the Hefley citation (line 202).} 

15. Equation 1: Please use proper statistical notation. For example, $\hat{y}$ should be E(y).

\reply{Thanks for the careful review. However, in this case, we define $\hat{y}$ as ``the predicted response,'' justifying the use of our notation.}

16. Lines 189–190: I think this statement about Bayesian statistics is going to be more confusing than helpful. Also, it is such a minority view in modern statistics that it isn’t worth paying much attention to.

\reply{On this point we disagree because several informal reviewers asked that we mention this issue. We have decided to leave this text in the manuscript.}

17. Line 202: Replace “Data summary” with “statistic” or “function of the data,” which are the proper terms for what you’re talking about.

\reply{We have changed to ``summary of the data'' (line 249). While ``statistic'' is the correct terminology within mathematical statistics, ecological statistics often refers to data summaries, for better or for worse. Note that we do define a couple of specific ``statistics'' in this same sentence.}

18. Lines 234–236: I have no clue what the authors are stating as a fact here.

\reply{We have edited this sentence to say: ``AIC is more forgiving of possibly spurious covariates than NHST, leading to models with more covariates, because of the asymmetry between the effect of omitting a relevant covariate and the effect of including a spurious one.'' We elaborate on this point in the next three sentences. See all of lines 287-298.}

19. Line 264 and elsewhere: “smoothing parameter” is uncommon except in the semi-parametric literature. Wouldn’t “regularization parameter” or another common term (e.g., tuning parameter).

\reply{Done. Changed to ``regularization parameter'' throughout.}

20. Line 281: I took a look at the Hooten et al. (2019) and there is nothing about anything the authors talk about in that paper. I think they meant to cite Hooten and Hefey (2019), but even this source is questionable because it is not a review.

\reply{Apologies, this was a mis-citation. Good catch. It has been removed.}

21. Lines 290–292: I don’t think this is good advice. It is too vague to be helpful and will lead to more confusion. For example, how does one ensure the data are as “independent as possible.” Without giving ecologists the technical reasons why this matters it doesn’t seem reasonable to dish out a few sentences of advice and in hope that somehow they will figure out how to use it.

\reply{We respectfully disagree that this is bad advice; see the cited paper by Roberts et al. (2017). However, we have now included a (hopefully) clearer explanation of why this is important (line 344).}

22. Line 303: Wood (2017) seems like a inappropriate reference for cross-validation and approximations to cross-validation scores. There isn’t much in this book about either of those topics since the focus is on generalized additive models. Please give page number if I missed something.

\reply{Good catch. We have changed the citation to Hastie et al. (2009). See line 356.}

23. Equation 4: Doesn’t Rt and Nt need a subscript m to denote the subpopulation?

\reply{Good catch. Fixed.}

24. Equation 4: Again, this isn’t a statistical model unless you add distributional assumptions. Please put E(Rt) on the right hand side and specify what distribution the expected value refers to. Conversely you could use notation like Rt = ß0,m + ... + et and give that distributional assumption for et.

\reply{Sorry for our sloppiness. Fixed.}

\end{document} 