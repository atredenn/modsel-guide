Dr. Peter Adler
Utah State University
5230 Old Main Hill
Logan
Utah
United States
84322



Leipzig, 09-Mar-2020



Dear Dr. Adler,

Manuscript number: ELE-00133-2020
Title: A practical guide to selecting models for exploration, inference, and prediction in ecology
Author(s): Tredennick, Andrew; Hooker, Giles; Ellner, Stephen; Adler, Peter


We have now received the Referees' reports on your manuscript, which accompany this letter.

Based on these reports, I am sorry to say that we are declining your manuscript for publication consideration in Ecology Letters. Although I realize that you could probably address many of the comments in a revision, the overall nature of the reviews is such that a revised study would not compete for the limited space in Ecology Letters. I am afraid therefore that we are not willing to consider a resubmission.

Page space at Ecology Letters is extremely competitive. We aim to publish the most innovative research in the ecological sciences, which necessitates selection on scientific quality, novelty and generality. Despite the fact that most manuscripts sent for external review receive positive assessments, we can only publish those few attracting the strongest support. Although we regret that many contributors are likely to be disappointed by our selectivity, we ensure that all of our decisions are made rapidly. As such, declined manuscripts can be submitted with little lost time to alternative publishing venues.

We hope that despite our decision, the efforts made by these reviewers will be used to their fullest in revising your manuscript for submission to an alternative journal.

Although Ecology Letters cannot publish your paper, I think your paper is a candidate for publication in Ecology and Evolution. I would like to offer you the opportunity to transfer your paper (and the associated reviews) automatically to Ecology and Evolution. Please note that acceptance of the offer to transfer does not guarantee acceptance of your manuscript as the Editors of Ecology and Evolution will still need to undertake due evaluation of the manuscript. Once the transfer of the files, data and reviews is complete you will receive an email request from Ecology and Evolution asking you to log in to the submission site's Author Center, provide any missing information unique to Ecology and Evolution's submission process, and complete the submission.

If you agree to the automatic transfer your paper and related reviews to Ecology and Evolution then click the link below:

*** PLEASE NOTE: This is a two-step process. After clicking on the link, you will be directed to a webpage to confirm. ***

https://mc.manuscriptcentral.com/ele?URL_MASK=1f5c6ede0c03482b8af6014076f02959



Yours sincerely,

Dr. Jonathan Chase
Senior Editor
Ecology Letters


******************************************
Referees' comments to the author(s):

Referee: 1

Comments for the Authors
Overview and strengths

Straight forward guide book for model selection at different points in the research process. This review paper seeks to describe possible approaches to model selection for exploration, inference, and prediction. Then, this paper uses a simple example of butterfly population data to show how these approaches may be applied. I think this is a strong manuscript and I have no major comments on the text. I appreciate that the authors give their experience as an example of why this manuscript is needed. I imagine that this manuscript will be particularly useful for early career ecologists who are looking for an entry point to guide early research projects. I think the somewhat casual language helps make this paper more accessible. But, the authors might consider reducing the amount of text about their personal experiences because it makes it feel more like opinion than fact.

Minor comments

line 29: �indeed, � -> insert space

line 59: unmanageable? with today's access to thousands of computer cores?

line 69: not sure I understand why a priori knowledge of covariates would be limited at this point in reading

line 84: I might pivot here to �model selection is an active field of research�

line 114: put footnote at the end

line 119: reference

line 164: a little confusing that you are missing y_hat here. Maybe say y_hat is the prediction given the max lik ests of the regression coefficients

line 511: 'on each modeling goal'?

Referee: 2

Comments for the Authors
Andrew Tredennick and colleagues offer some interesting ideas about the need to consider goals for achieving insight as a part of model selection, arguing that different models might be �best� for different purposes: prediction, inference, and exploration. That point is the single novel contribution of the paper, and I am not convinced it is terribly novel. Much has been previously written for example, about the hands-down superiority of machine learning for developing predictive models that reveal nothing about mechanisms.

The paper suffers from redundancy with existing treatments of model selection and, alas, at the same time fails to treat issues that are truly central to any discussion of this topic. The discussion of regularization, model validation, and information criteria are treated more thoroughly and rigorously in other recent review papers. Other vitally relevant topics, notable among them model checking, the Bayesian probability of the model, and indicator variable selection, are surprisingly absent. The authors fail to recognize that prediction and inference are treated identically in the Bayesian framework � model predictions and parameters are both treated as random variables (as are all other unobserved quantities). The failure to even mention this property weakens the distinction between prediction and inference that forms the main motivation for the paper. The authors overlook the critical topic of �ignorability�, that is the linkage between the way that data were collected and the structure of the model required for inference. Model selection is rarely appropriate for designed experiments, for example, where the design specifies the model that must be used for inference.

The authors seem to believe that model selection of some type is a requirement for insight. Not so. Often, conditioning on a single model is all that is needed. One model suffices nicely if it is known that a set of predictor variables have mechanistic influences on a response, and the investigator seeks to understand the relative strength of those influences.

In conclusion, there is not much new in this paper. There are topics that should be covered in a review of this type that are omitted.

Referee: 3

Comments for the Authors
I enjoyed reading �A practical guide to selecting models for exploration, inference, and prediction in ecology� as submitted to Ecology Letters. The authors� exposition of why different goals require different tools for model selection should be useful to the many ecologists who fit models to data. The example exercises for each of the goals � exploration, inference, and prediction � using butterfly population responses to weather were clear, simple and compelling. I�ve only one small addition and a handful of other comments that the authors might consider.

I appreciated the call to think critically when fitting models to data. And recognising that the focus is on the three different goals and how model selection might be tailored to suit each, I still missed some mention of the difficulties of moving between hypotheses and models, particularly when modelling for inference. For example, a recent textbook by Richard McElreath �Statistical Rethinking� (that will be updated this year), emphasises how hypotheses do not always map simply and directly through process-based models to statistical models (Fig. 1.2). In the �Guidance� section for modelling for inference, I missed the recognition of why critical thinking is required to move between hypotheses and models. Discussing that hypotheses do not always imply unique models, and that models do not always imply unique hypotheses is probably beyond the scope of the current presentation, but I missed an acknowledgment of the difficulties associated with formulating a set of candidate models directly linked to a priori hypotheses.

McElreath, R. 2016. Statistical Rethinking: A Bayesian course with examples in R and Stan. CRC Press.

Other comments:

L137: �meaning the generation <i>but</i> not the testing of hypotheses�

L223: does this sentence need a qualifier? Including spurious covariates will have a minimal impact on <i>within-sample</i> predictions when data are sufficient for good parameter estimates?

L327: the second �relationship� should be plural too.

L333: random intercept only?

L342-347: I appreciated the interrogation of the coefficients: including the focal parameter names in parentheses would make it easier to find them in the table.

L367: change �same� to �similar� here. I got bit confused as to whether all the weather covariates were included in this analysis. The figure caption clarified that the analysis only looked at mean temperature. I guess �same� could refer to the drop-one process of dropping the mean temperature for a given window and comparing it to the no-climate null. A small edit here to clarify how exactly this analysis differs (or doesn�t) from the earlier exploration would help readers.

L482: Fig 7B

Line 514-515: Table 3 looks like tools for model and variable selection, not just exploration.

Table 2: not sure why model selection has only a minimal emphasis when using models for inference? For example, inferring the importance of the snowpack x temperature interaction is impossible without it. That model selection (here called NHST and formulated as a likelihood ratio test) is required to make inferences from a competing set of models means that the importance of model is selection is greater than �minimal�.


Referee: 4

Comments for the Authors
This paper reads provides a very readable introduction to frequentist model selection, covering several well-established methods with the tricky problem of selecting weather-related covariates for ecological dynamics as a case study. Perhaps the biggest contribution of the work is a very clear delineation between exploration, inference, and prediction, that I think many will find useful. I also appreciated how the authors revisited some of their previous works (e.g. Dalgleish et al. 2011) in the context of the new clarity of their current framework.

My main criticism is that much of the material in the paper feels dated or irrelevant in an age where hierarchical, spatial, and Bayesian models are increasingly common. There are several notable examples of  missing recent advances.

1) An enormous problem wtih AIC is that counting the number of parameters  (p in equation (2) ) is nearly impossible for models that involve partial pooling, for example, any random effects models. In response to this (and other problems with AIC), many contemporary solutions have been developed. These include the widely-applicable information criterion (WAIC), and various other LOO-IC criteria that avoid problems with AIC (see Aki Vehtari's recent papers, as well an the older review by Hooten and Hobbs (already cited in manuscript) for a thorough discussion). I understand that part of the motivation for ignoring these techniques is to focus on what most ecologists are currently implementing. But I think papers in Ecology Letters, and quantitative ecologists in general, should be pushing boundaries and explaining concepts on the forefront, rather than spending time elucidating methods we already know are flawed.

2) The paper is almost entirely focused on frequentist statistics. Several of the authors have published Bayesian analyses that I greatly admire, so I found this focus very puzzling. I suspect one reason is to make the paper more accessible to beginning ecologists (?). The problem is, Bayesian methods are more accesible than ever before, both from a programming view point and as a result of several new and easy-to-read conceptual introductions. For example, the rstanarm package has syntax that parallels lme4 and is as easy to use. Richard McElreath's "Rethinking statistics" is a good overview of Bayesian stats suitable for non-mathematically inclined readers. There are many places in the text where Bayesian methods would be appropriate to mention and use. Here are few examples:

 �Regularizing priors in Bayesian analyses present a straightforward way to implement regularization.  This is described in the Supplemental material, but should also be discussed in the main text.

 �Posterior predictive distributions present a straightforward way to propagate uncertainty in forecasts, again mentioned in an off-handed way but could be more central to manuscript.

�Going back to point (1), modern Bayesian methods (e.g. looic) present alternatives to AIC that avoid many of the assumptions of that criterion.

Concluding comments: In my view, the strongest aspect of this paper is conceptual stats guidance and reflection that is not tied to a particularly methodology (discussion of three main goals of ecology and reflection on case studies). One way forward could be to emphasize those more qualitative aspects and cut some of the more dated methods from the main text.

******************************************
Editor
Editors Comments for the Author(s):
We now have four reviews for this manuscript, with a difficult split between them. Two being quite favorable, and two being quite critical. In addition, with the rejection of the original manuscript by a specialist editor who felt that the work was interesting, but not novel enough for Ecology Letters, I�m afraid I must recommend rejection. In all, this manuscript seems like a nice treatment for students (e.g., with clear delineation and description of exploration, inference and prediction), but seems to be insufficiently deep, nor at the cutting edge in terms of the technical tools identified and discussed. For example, although I appreciate the coherent formalisation of exploration, inference and prediction, the model selection recipes seem well known. Likewise, the concerns of R2 provides alternate and clear examples of situations for when multiple models for inference can be inappropriate. Further, the concerns of both R2 and R4 about the lack of discussion of Bayesian approaches (where e.g., both prediction and inference can be identical) highlights the nagging feeling that while interesting and quite readable, the manuscript does not meet the criteria for really pushing the �cutting edge� of synthesis.
CAUTION: This email originated from outside of USU. If this appears to be a USU employee, beware of impersonators. Do not click links, reply, download images, or open attachments unless you verify the sender�s identity and know the content is safe.
