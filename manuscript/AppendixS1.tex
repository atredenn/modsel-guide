
%---------------------------------------------
% This document is for pdflatex
%---------------------------------------------
\documentclass[11pt]{article}

\usepackage{amsmath,amsfonts,amssymb,graphicx,setspace,authblk}
\usepackage{float}
\usepackage[running]{lineno}
\usepackage[vmargin=1in,hmargin=1in]{geometry}
\usepackage[sc]{mathpazo} %Like Palatino with extensive math support

\usepackage[authoryear,sort]{natbib}

\graphicspath{ {../figures/} }

\usepackage{enumitem}
\setlist{topsep=.125em,itemsep=-0.15em,leftmargin=0.75cm}

\usepackage{gensymb}

\usepackage[compact]{titlesec} 

\usepackage{bm,mathrsfs}

\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\usepackage{epstopdf}
\else
\DeclareGraphicsExtensions{.eps}
\fi

\renewcommand{\floatpagefraction}{0.98}
\renewcommand{\topfraction}{0.99}
\renewcommand{\textfraction}{0.05}

\clubpenalty = 10000
\widowpenalty = 10000

\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\ba}{\begin{equation} \begin{aligned}}
\newcommand{\ea}{\end{aligned} \end{equation}}

\def\X{\mathbf{X}}

\floatstyle{boxed}
\newfloat{Box}{tbph}{box}

\title{\textbf{Supporting information for review and publication for: A practical guide to selecting models for exploration, inference, and prediction in ecology}}

\author[1]{Andrew T. Tredennick}
\author[2]{Giles Hooker}
\author[3]{Stephen P. Ellner}
\author[4]{Peter B. Adler} %\thanks{Corresponding author.  Email: peter.adler@usu.edu}}
\affil[1]{Western EcoSystems Technology, Inc., Laramie, WY, USA}
\affil[2]{Department of Statistics and Data Science, Cornell University, Ithaca, NY, USA}
\affil[3]{Department of Ecology and Evolutionary Biology, Cornell University, Ithaca, NY, USA}
\affil[4]{Department of Wildland Resources and the Ecology Center, Utah State University, Logan, Utah.}


\renewcommand\Authands{ and }

%\date{}

\sloppy

\renewcommand{\baselinestretch}{1.25}

\begin{document}
	
	\maketitle


%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% APPENDICES !
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\setcounter{page}{1}
\setcounter{equation}{0}
\setcounter{figure}{0}
\setcounter{section}{0}
\setcounter{table}{0}

\renewcommand{\theequation}{S\arabic{equation}}
\renewcommand{\thetable}{S\arabic{table}}
\renewcommand{\thefigure}{S\arabic{figure}}
\renewcommand{\thesection}{S\arabic{section}}

\section{Supporting tables}

\begin{table}[ht]
	\centering
	\caption{\label{tab:undstd-mod-table} Parameter estimates from the \emph{a priori} specified model for
		inference about effects of winter weather on butterfly population growth.
		\label{tab:understand-tab}}
	\smallskip
	\begin{tabular}{rrrr}
		\hline
		& Estimate & Std. Error & $t$ value \\ 
		\hline
		(Intercept) & 2.32 & 0.29 & 8.09 \\ 
		logNt & 0.55 & 0.05 & 10.24 \\ 
		Snow & -0.02 & 0.00 & -5.38 \\ 
		Temperature & -0.39 & 0.06 & -6.88 \\ 
		Snow$\times$Temperature & 0.01 & 0.00 & 5.22 \\ 
		\hline
	\end{tabular}
\end{table}


\section{Statistical regularization}\label{regularization}

Regularization methods other than information-theoretic criteria are unfamiliar to many ecologists, so we provide a brief review of three prominent techniques: ridge regression,
LASSO (least absolute shrinkage and selection operator), and the elastic net. Statistical regularization is exceptionally useful for selecting
optimally predictive models \citep{Gerber2015,Hooten2015}, which explains the common use of regularization in the machine learning literature \citep{Hastie2009,Sra2012}.

Regularization is formally expressed as
\begin{equation}
\hat{\theta} = \underset{\theta}{\text{argmin}}\left( \mathcal{L}(\textbf{y},\bm{\theta}) + r(\bm{\theta},\gamma) \right),
\end{equation}

\noindent{}where $\mathcal{L}(\textbf{y},\bm{\theta})$ is a loss function that depends on the data ($\boldsymbol{y}$) and model parameters ($\bm{\theta}$), and $r$ is the penalty function.
Typical loss functions are a sum of squared errors, negative log Likelihood, deviance, or posterior predictive density.
The notation $\underset{\theta}{\text{argmin}}(g)$ reads, "minimize function $g$ with respect to $\theta$."
Statistical models are generally fitted by optimizing some loss function.
But optimizing the loss function without constraint can lead to overfit models that generalize poorly beyond the data used to fit the model.
Regularization guards against overfitting by offsetting the minimization of loss with a regulator function, $r$, which is a function of the model
parameters ($\bm{\theta}$) and other variables ($\gamma$) that control the form and severity of the penalty imposed by $r$.

In a multiple regression context, where our model takes the form $y_i = \mbox{Normal}\left( \beta_0 + \sum_{j=1}^p x_{ij}\beta_j, \sigma^2 \right)$, we seek
to find the estimates for $\beta_0$ and $\bm{\beta} = (\beta_1,\dots,\beta_p)$ that minimize the sum of squared errors.
Thus, our loss function is
\begin{equation}
\mathcal{L}(\textbf{y},\beta_0,\bm{\beta}) = \sum_{i=1}^n \left(y_i - \beta_0 - \sum_{j=1}^p x_{ij}\beta_j\right)^2,
\end{equation}
\noindent where the estimates of model parameters are completely unconstrained by a regulator.
To guard against overfitting, we can introduce a penalty via a regulator function such that the quantity we seek to optimize is instead
\begin{equation}
\underbrace{\sum_{i=1}^n \left(y_i - \beta_0 - \sum_{j=1}^p x_{ij}\beta_j\right)^2}_{\textbf{Loss}} +\gamma_1\underbrace{ \sum_{j=1}^p |\beta_j|^{\gamma_2}}_{\textbf{Regulator}}
\label{eqn:regFun}
\end{equation}
where $\gamma_1$ is referred to as the ``penalization'' parameter and $\gamma_2$ is the ``degree of the {'norm' ''} \citep{Hooten2015}.
$\gamma_1$ determines the strength of the penalty on a coefficient with value 1, and $\gamma_2$ determines rate at which that penalty is applied as the coefficient increases from zero.
In the statistical literature, $\gamma_1$ is typically denoted by $\lambda$, which is varied to explore the effect of regularization,
while $\gamma_2$ is typically denoted simply by $\gamma$, and is held constant within an approach\footnote{Our notation
	follows Hooten and Hobbs (2015), who avoid using
	$\lambda$ to avoid confusion with notation from the
	population modeling literature where $\lambda$ refers
	to the population growth rate.}.
This will become clear shortly in our descriptions of ridge regression, LASSO, and elastic net, each of which takes the form of Eq. \ref{eqn:regFun}.
The regulator function in Eq. \ref{eqn:regFun} is only one example of a penalty function.
Many others are possible, especially for models other than linear regression \citep{Sra2012}.

It is important to note that the regularized estimates of $\hat{\boldsymbol{\beta}}$ will be
different from the maximum likelihood estimates from an unregularized optimization; we are accepting some
bias in the estimates in return for reduced variance (Fig. \ref{reg-ex}) and (hopefully) a smaller mean-square prediction error.
This is desirable because regularization is about generality and prediction (i.e., concerned with $\hat{\mathbf{y}}$) rather than understanding as we have
defined it (i.e., concerned with $\hat{\boldsymbol{\beta}}$) \citep{Mullainathan2017}.
In what follows we describe the main strengths and weaknesses of three common regularization techniques, but refer readers to
\cite{Frank1993} for detailed comparison of several regularization techniques.

\begin{figure}[tbp]
	\centering
	\includegraphics[width=\textwidth]{new_regularization_plot.pdf}
	\caption{Conceptual figure of how LASSO and Ridge Regression work, for the case of a single parameter $\beta_1$.
	In both panels, the dash-dot red curve shows the Loss function term in eqn. \eqref{eqn:regFun} as a function of the coefficient
	estimate. Without the Regulator term, the parameter estimate would be the value of $\beta_1$ at which this curve is minimized
	(open red circle). The dotted curve shows the Regulator function, $|\beta_1|$ for LASSO and $\beta_1^2$ for Ridge Regression.
   When $\gamma_1=1$ eqn. \eqref{eqn:regFun} equals Loss + Regulator (solid purple curve) and the parameter estimate is shrunk towards
   zero (solid purple circle), moreso by LASSO in this example though that is not always the case. When $\gamma_1=2$  eqn. \eqref{eqn:regFun}
   equals Loss + 2*Regulator (dashed purple curve) and the parameter estimate (solid purple square) is even closer to zero. The kink at zero in
   LASSO regulator function results in the parameter estimate being reduced to zero. Because the Ridge Regression regulator function is flat at zero,
   Loss + $\gamma_1$*Regulator is not flat at zero for any value of $\gamma_1$, so the parameter estimate is never exactly zero. }
\label{reg-ex}
\end{figure}

\subsection{Ridge regression}
\label{ridge-regression}

Ridge regression aims to minimize
\begin{equation}
\sum_{i=1}^n \left( y_i - \beta_0 - \sum_j^p x_{ij}\beta_j \right)^2 + \gamma_1 \sum_j^p |\beta_j|^2,
\label{eqn:ridge}
\end{equation}
\noindent which is a particular instance of Eq. \ref{eqn:regFun}
where $\gamma_2 = 2$ \citep{Hoerl1970}. Looking at the
second term in Eq. \ref{eqn:ridge} reveals why ridge regression falls
under the category of ``shrinkage'' methods. If the $\beta_j$s take on
large values, then the regulator function,
$\gamma_1 \sum_j^p |\beta_j|^2$, will also be large, strongly
penalizing the optimization function. Thus, for a given value of
$\gamma_1$, the optimization is biased toward smaller values of the
$\beta_j$s; the regulator shrinks the estimates toward 0. At
$\gamma_1 = 0$, there is no regularization and the $\beta_j$s take the value
that minimizes the loss function (e.g., the maximum likelihood estimates).
As $\gamma_1$ increases, the $\beta_j$s take smaller and smaller values until they are all near
zero (at $\gamma_1 = \infty$, $\beta_j \approx 0$ for all $j$,
though this occurs well before reaching $\gamma_1 = \infty$, for which
our computers are thankful). Because $\beta_j$s are never shrunk all the
way to zero, ridge regression is not a variable selection method; all
covariates are retained when coefficients are regulated by ridge
regression, though many may be close to zero.

In Bayesian statistics, ridge regression and other regularization
techiniques can be implemented by adjusting the prior distribution for
unknown parameters \citep{Hooten2015}. For example, let
$\text{Normal}(0,\sigma^2)$ be the prior distribution for some
unknown parameter $\beta$. We can constrain the posterior distribution
of $\beta$, and shrink it toward zero, by setting $\sigma^2$ to a
small value. In contrast, if we want the prior to be ``uninformative''
(or vague), we can set $\sigma^2$ to a large value. For ridge
regression, we can fit a model across a grid of $\sigma^2$ values,
from small to large, to find the optimal variance for the prior \citep{Gerber2015,Hooten2015,Tredennick2017a}.
Note that ridge regression requires the use of a Normal prior distribution; other
techniques use different distributions. We discuss how to find the
optimal penalty, or prior, in the section on \emph{Model validation}.

\subsection{Least absolute shrinkage and selection operator
	(LASSO)}\label{least-absolute-shrinkage-and-selection-operator-LASSO}
The Least absolute shrinkage and selection operator (LASSO) \citep{Tibshirani1996}
is similar to ridge regression, but is slightly more severe in
that it simultaneously shrinks estimates and performs variable
selection by shrinking coefficient estimates all the way to zero (unlike
ridge regression).
% Ridge regression shrinks estimates of $\beta_j$ toward
%zero, but the estimates never become exactly zero. %The LASSO achieves
%variable selection because the penalty is applied linearly, rather than
%nonlinearly as in ridge regression (Fig. \ref{reg-ex}). The linear
%application of the penalty ($\gamma_1$) occurs because
%$\gamma_2 = 1$, yielding the minimization function
The LASSO corresponds to the minimization problem
\begin{equation}
\sum_{i=1}^n \left( y_i - \beta_0 - \sum_j^p x_{ij}\beta_j \right)^2 + \gamma_1 \sum_j^p |\beta_j|.
\label{eqn:lasso}
\end{equation}
%\noindent{}meaning that estimates of $\beta_j$ becomes exactly zero as
%
\noindent Because the penalty on each coefficient in the second term has a
sharp ``corner'' at $\beta_j=0$ (Fig. \ref{reg-ex}), the estimate of $\beta_j$ can be
set to exactly zero when the sum of the two terms is minimized at $\beta_j=0$.

The LASSO is widely used and performs similarly to ridge regression,
except in cases where predictors are collinear. For example, the LASSO
tends to pick only one of a group of correlated predictors, and that
selection is inconsistent across replicate data sets \citep{Zou2005}. If $x_1$ and $x_2$
are highly correlated, LASSO might choose to set $\beta_1 = 0$ in one
round of model fitting and $\beta_2 = 0$ in the next set. Ridge
regression has its own quirks with correlated predictor
variables -- it tends to ``spread out'' effects across correlated
predictors such that several correlated predictors end up with small and
relatively equal coefficient estimates. Ridge regression generally
outperforms LASSO when the number of observations is greater than
the number of covariates ($n > p$) \citep{Tibshirani1996,Zou2005}.

As with ridge regression, the LASSO can be implemented in Bayesian
statistics through the prior distribution on unknown parameters. Where
ridge regression uses a Normal distribution, the LASSO uses
the Laplace distribution for the prior. The Laplace has a sharp peak at
zero, which implements the desired feature of the LASSO drawing
parameters toward zero at a steeper rate than ridge regression (also see
eqn. 31 in \citealt{Hooten2015}). Markov chain Monte Carlo (MCMC)
algorithms for fitting Bayesian models will never result in variables
being exactly zero, but the mode of the posterior distribution for each
parameter can be used to identify parameters that should be zero versus
nonzero.

\subsection{Elastic net}\label{elastic-net}
The elastic net \citep{Zou2005} is a combination of ridge
regression and the LASSO that seeks to minimize
\begin{equation}
\sum_{i=1}^n \left( y_i - \beta_0 - \sum_j^p x_{ij}\beta_j \right)^2 + \gamma_3 \sum_j^p \beta_j^2 + \gamma_1 \sum_j^p |\beta_j|,
\label{eqn:enet}
\end{equation}
\noindent where the special case $\gamma_3 = 0$ is the LASSO and
the special case $\gamma_1 = 0$ is ridge regression. The regulator
function for the elastic net is typically rewritten as,
\begin{equation}
\gamma \sum_{j=1}^p\left[\frac{1}{2}(1-\alpha)\beta_j^2 + \alpha|\beta_j| \right]
\label{eqn:enet-func}
\end{equation}
\noindent where $\alpha$ serves as a mixing parameter that
between ridge regression ($\alpha = 0$) and LASSO ($\alpha = 1$).
We show both formulations of the elastic net penalty because Eq. \ref{eqn:enet} is
easier to relate to the LASSO and ridge regression penalty expressions, but
software implementation of the elastic net usually involves the $\alpha$ in
eqn. \eqref{eqn:enet-func} as a parameter for the user to specify
(e.g., \citealt{Friedman2010}). The elastic net penalty represents a
compromise between sparsity and variable inclusion. Ridge regression
never excludes variables but strongly shrinks their coefficients toward zero.
The LASSO penalty excludes variables but does not shrink the coefficients for the
remaining variables as much as the ridge penalty. By combining
the best features of ridge and LASSO (when $0 < \alpha < 1$), the
elastic net has proven to be especially useful when the number of
predictors is far greater than the number observations ($p \gg N$) or when
many predictors are correlated \citep{Friedman2010}.

Because the elastic net is a combination of penalties from ridge
regression and the LASSO, it is implemented in Bayesian statistics with
a prior distribution that is a compromise between Normal and Laplace.
Showing the derivation of the prior distribution for the Bayesian
elastic net is beyond the scope of this paper, so we point interested readers to \citet{Li2010},
who first proposed the method.

\subsection{Regularization of mixed-effects
	models}\label{regularization-of-mixed-effects-models}

Mixed effects models can also be regularized, usually by adding LASSO, Ridge or elastic net penalties to the mixed effects log likelihood; see \citet{bondell2010joint}, and
packages \texttt{lmmlasso} \citep{schelldorfer2011estimation}, and \texttt{lmmem} \citep{sidi2017}, also \texttt{bigtime} \citep{wilms2017bigtime} in a
time-series context. However, the statistical literature on these methods is sparse in comparison to studies of regularized regression models.

Within mixed effects models, choosing regularization parameters and/or evaluating predictive performance requires dividing the data into nearly independent blocks so that no information is shared between the data used to estimate parameters and the data used to evaluate performance. In a simple framework such as repeated measures, this means leaving out the experimental units that correspond to levels of a random effect as we have done with years in our work. In more complex models such as those with crossed or nested effects,  or where the correlation structure extends over all the data, such as with time series, it can be more difficult to work out a reasonable cross-validation scheme.

When evaluating out-of-sample performance, the researcher also faces the question of whether to impute new random effects for the subjects in the test set, or whether to make predictions at the average random effect value. Imputing new random effects allows a reasonable comparison of log-likelihoods between training and test data, while making predictions at the average random effect value corresponds to predictions made for unknown data.

\section{Model validation}\label{model-validation}

Model validation is how we answer the question, ``How good is my
model?'' There are many metrics for measuring model performance and
goodness-of-fit. Ecologists are probably most familiar with metrics such
as the coefficient of determination ($R^2$), root mean square error
(RMSE), mean square error (MSE), and mean absolute error (MAE). These
are all related due to their dependence on squared residual errors,
$\left(\hat{y}_i - y_i\right)^2$, and we suspect $R^2$ is commonly
reported in manuscripts because nearly every statistical package in
every computing environment spits that value out. $R^2$ is a good
heuristic for model quality, but it only tells us about the closeness of
within-sample predictions to within-sample data, which means
that $R^2$ will continually increase as parameters are added
to the model. This may be acceptable if your goal is understanding and
the model set is small, but it is probably far from ideal if your goal
is out-of-sample prediction. There is also the
``adjusted $R^2$,'' which penalizes (slightly) for extra parameters,
in order to give an unbiased estimate based on in-sample data of the amounts of variance
explained and unexplained by the model, so long as the model is unbiased.
However, the penalty per parameter is not large enough for
adjusted $R^2$ to decrease, on average, when spurious covariates are
added to the model, so it is not a good criterion to select a model
for prediction accuracy.
Model validation relies on calculating metrics or scores like RMSE, MSE, MAE, etc. from predicting
observations that were not used to fit the model (``out of sample predictions''). The squared errors
used to evaluate the model are then $\left(\hat{y}_i - y^\text{oos}_i\right)^2$,
where \emph{oos} means out-of-sample.

\subsection{Out-of-sample validation}\label{out-of-sample-validation}

When prediction is the goal, out-of-sample validation is the gold
standard for assessing a model's skill. Out-of-sample validation is
important because there may be patterns within the data used to fit the
model that are not actually prevalent outside that particular sample. For
example, imagine we have collected yearly estimates of abundances of
\emph{Aedes aegypti} mosquitos in the southeastern United States with
the proximate goal of relating interannual variation in average winter
temperature to \emph{Ae. aegypti} abundance and growth rate. Our
ultimate goal is to forecast \emph{Ae. aegypti} abundance across its
range based on projected weather in future years, thus potentially
telling us something about the risk of Zika virus outbreaks since
\emph{Ae. aegypti} is the primary vector of Zika. Our model, fit using
data from the southeastern U.S. only, includes a significant effect of
mean November through January temperature. The range of \emph{Ae.
aegypti} extends far north of where we collected our data, however, and
the time period over which temperature most affects population growth
might be extended due to longer periods of cold weather as we move
north. Thus, our fitted model might be able to predict abundances in the
southeast relatively well, but fails to predict abundances farther
north. In-sample validation would not have told us we need a different
model; out-of-sample validation on more northern observations
would have caught the problem quickly.

So how does out-of-sample validation work? The starting point is to
split a data set, $\mathcal{D}$, into training and validation sets using appropriate randomization.
The size of each set depends on the size of the data set. If the
data set is relatively small, we might hold out only
15-25\% of the data for validation. If the data set is relatively large,
we might hold out 25-50\% of the data for validation. The
more data we can afford to hold out, the better the test for
out-of-sample prediction.\footnote{In the machine learning community,
	where many data sets are much, much larger than those typically encountered
	by ecologists, the splitting of $\mathcal{D}$ actually goes a step
	further to include a third split, the test set,
	$\mathcal{D}^\text{test}$. This is done because
	$\mathcal{D}^\text{oos}$ is used as part of the model selection
	process and may therefore still bias the model toward that data. So
	$\mathcal{D}^\text{test}$ is used to calculate the predictive skill
	of the final chosen model.}
We will call these data splits $\mathcal{D}^\text{train}$ (training data) and
$\mathcal{D}^\text{oos}$ (out-of-sample data). While the data
splitting should be done randomly, the randomization procedure can be
stratified to account for temporal, spatial, or other structure in the
data \citep{Roberts2017}. For example, if you are fitting a temporally
structured model, such as population counts through time, with multiple
observations at each time point, then data splitting should be
stratified by time. That way, the held-out data all come from a year that is completely
unused in fitting the model.

The split data sets each contain vectors of responses
($\mathbf{y}^\text{train}$, $\mathbf{y}^\text{oos}$) and matrices of
predictors/covariates ($\mathbf{X}^\text{train}$,
$\mathbf{X}^\text{oos}$). We then fit candidate models using
$\mathbf{y}^\text{train}$ and different combinations of the columns of
$\mathbf{X}^\text{train}$. Each candidate model is then used to make
predictions ($\hat{\mathbf{y}}^\text{train}$) based on the
out-of-sample covariates $\mathbf{X}^\text{oos}$. Thus, for each
candidate model \emph{M} we have a vector of predictions,
$\hat{\mathbf{y}}^\text{train}$, that we can compare to the
out-of-sample observations, $\mathbf{y}^\text{oos}$. For example, we
could calculate the mean absolute error (MAE
$\equiv \frac{1}{N}\sum^N_{i=1}\left(|\hat{y}_i^\text{train} - y^\text{oos}_i| \right)$)
and then rank each model \emph{M} by its out-of-sample MAE. The model
with the lowest MAE would be the optimally predictive model.

Out-of-sample validation is useful for many tasks. In the abstract
example in the previous paragraph, out-of-sample validation was used to
select among competing models. Out-of-sample validation is also
extremely useful for selecting the optimal penalty for statistical
regularization. For example, we can fit the same model across a grid of
penalty strengths and rank the penalties by out-of-sample score \citep{Gerber2015,Hooten2015}.
Lastly, out-of-sample validation
can be used independent of model selection as a measure of a fitted
model's predictive skill.

\subsection{Cross-validation}\label{cross-validation}

Cross-validation is the workhorse for selecting optimally predictive
models using small data sets. Out-of-sample validation requires data
sets large enough that splitting it apart leaves enough
observations in the training set to reliably fit a model, and the test set
is large enough to represent all situations where an overfitted or underfitted
model will fail. Often in ecology, the data set
is so small that we do not want to jettison any information by
holding out data completely. This is where cross-validation comes into play.

In out-of-sample validation the data set $\mathcal{D}$ is split once,
and only once, into training and validation sets. In cross-validation
the data set $\mathcal{D}$ is split multiple times into training and
validation sets. The data splits in cross-validation are often referred
to as \emph{folds}, such that \emph{K}-fold cross-validation means we
create \emph{K} folds of randomly split training
($\mathcal{D}_k^\text{train}$) and validation sets
($\mathcal{D}_k^\text{oos}$), where the observations that compose the
training and validation sets vary by fold. For each fold \emph{k} in
$1,2,\dots,K$ we fit the model to a training set, make predictions
using the validation set, and then calculate some score of model skill
(e.g., MAE). This leaves us with \emph{K} validation scores, over which
we can sum to obtain the cross-validation score. We can also examine the
distribution of validation scores, if \emph{K} is sufficiently large.

Cross-validation can be used in any situation where out-of-sample
validation would be used. But it is particularly useful for selecting
optimally predictive models by choosing among nested subsets of models
or by choosing the optimal penalty for a statistical regularization
technique. As with out-of-sample validation, to obtain a valid test of
out-of-sample forecasting it is often essential to stratify the splitting.
For example, when data are collected across multiple years and between-year
variability is of interest, the training data in each fold should consists
of all data from some subset of the available years, and the test data are
from years outside that subset. Cross-validation then
tests a fitted model's ability to make predictions about unobserved years.
If folds are chosen completely at random from the entire data set,
and some observations for every year are included in the training set,
then cross-validation would only assess a model's ability to predict within the
years represented in the training set.

\newpage
\renewcommand{\refname}{Literature cited}

\bibliographystyle{ecology}
\bibliography{lemonade_ms}

\end{document}

